import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/advices',
    name: 'Advices',
    component: () => import('../views/Advices.vue')
  }, 
  {
    path: '/advices/advice-page',
    name: 'AdvicePage',
    component: () => import('../components/AdvicePage.vue')
  },
  {
    path: '/courses',
    name: 'Courses',
    component: () => import('../views/Courses.vue')
  },
  {
    path: '/courses/course-page',
    name: 'CoursePage',
    component: () => import('../components/CoursePage.vue')
  },
  {
    path: '/courses/course-page/video-lesson',
    name: 'VideoLesson',
    component: () => import('../components/VideoLesson.vue')
  },
  {
    path: '/clubs',
    name: 'Clubs',
    component: () => import('../views/Clubs.vue')
  },
  {
    path: '/blog',
    name: 'Blog',
    component: () => import('../views/Blog.vue')
  },
  {
    path: '/blog/article',
    name: 'Article',
    component: () => import('../components/ArticlePage.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile.vue')
  },
  {
    path: '/consultation',
    name: 'Consultation',
    component: () => import('../components/ConsultationModal.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../components/Login.vue')
  },
  {
    path: '/review',
    name: 'ReviewModal',
    component: () => import('../components/ReviewModal.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
